
----local application = require "mjolnir.application"
----local hotkey = require "mjolnir.hotkey"
----local window = require "mjolnir.window"
----local fnutils = require "mjolnir.fnutils"
----local screen = require "mjolnir.screen"
----local geo = require "mjolnir.geometry"

--hotkey.bind({"cmd", "alt", "ctrl"}, "space", function()
  --mjolnir.reload()
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "M", function()
  --local win = window.focusedwindow()
  --win:maximize()
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "H", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0,0.5,1)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "J", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0,1,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "K", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0.5,1,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "L", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0.5,0,0.5,1)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "H", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0,0.5,1)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "N", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0.5,0.5,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, ".", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0.5,0.5,0.5,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "U", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0,0,0.5,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "P", function()
  --local win = window.focusedwindow()
  --local r = geo.rect(0.5,0,0.5,0.5)
  --win:movetounit(r)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, ";", function()
  --local win = window.focusedwindow()
  --local sc = win:screen()
  --local nextsc = sc:next()
  --local r
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "right", function()
  --local win = window.focusedwindow()
  --local f = win:frame()
  --f.x = f.x + 100
  --win:setframe(f)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "left", function()
  --local win = window.focusedwindow()
  --local f = win:frame()
  --f.x = f.x - 100
  --win:setframe(f)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "up", function()
  --local win = window.focusedwindow()
  --local f = win:frame()
  --f.y = f.y - 100
  --win:setframe(f)
--end)

--hotkey.bind({"cmd", "alt", "ctrl"}, "down", function()
  --local win = window.focusedwindow()
  --local f = win:frame()
  --f.y = f.y + 100
  --win:setframe(f)
--end)






-----------------------------------------------
-- Set up
-----------------------------------------------

local hyper = {"cmd", "alt", "ctrl"}
local hyper_shift = {"ctrl", "alt", "shift"}

hs.window.animationDuration = 0

-----------------------------------------------
-- hyper d for left one half window
-----------------------------------------------

hs.hotkey.bind(hyper, 'h', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y
        f.w = max.w / 2
        f.h = max.h
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- right one half window
-----------------------------------------------

hs.hotkey.bind(hyper, 'l', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x + (max.w / 2)
        f.y = max.y
        f.w = max.w / 2
        f.h = max.h
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- top half window
-----------------------------------------------

hs.hotkey.bind(hyper, 'k', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y
        f.w = max.w
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- bottom half window
-----------------------------------------------

hs.hotkey.bind(hyper, 'm', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y + (max.h / 2)
        f.w = max.w
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- fullscreen
-----------------------------------------------

hs.hotkey.bind(hyper, 'm', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y
        f.w = max.w
        f.h = max.h
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- top left one quarter window
-----------------------------------------------

hs.hotkey.bind(hyper, 'u', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y
        f.w = max.w / 2
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- for top right one quarter window
-----------------------------------------------

hs.hotkey.bind(hyper, 'o', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x + (max.w / 2)
        f.y = max.y
        f.w = max.w / 2
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- bottom left one quarter window
-----------------------------------------------

hs.hotkey.bind(hyper, 'n', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x + (max.w / 2)
        f.y = max.y + (max.h / 2)
        f.w = max.w / 2
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- bottom right one quarter window
-----------------------------------------------

hs.hotkey.bind(hyper, '.', function()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        f.x = max.x
        f.y = max.y + (max.h / 2)
        f.w = max.w / 2
        f.h = max.h / 2
        win:setFrame(f)
    else
        hs.alert.show("No active window")
    end
end)

-----------------------------------------------
-- Reload config on write
-----------------------------------------------

function reload_config(files)
    hs.reload()
end
hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reload_config):start()
hs.alert.show("Config loaded")

-----------------------------------------------
-- Hyper i to show window hints
-----------------------------------------------

hs.hotkey.bind(hyper, 'f', function()
    hs.hints.windowHints()
end)

-------------------------------------------------
---- Hyper hjkl to switch window focus
-------------------------------------------------

--hs.hotkey.bind(hyper, 'K', function()
    --if hs.window.focusedWindow() then
        --hs.window.focusedWindow():focusWindowNorth()
    --else
        --hs.alert.show("No active window")
    --end
--end)

--hs.hotkey.bind(hyper, 'J', function()
    --if hs.window.focusedWindow() then
        --hs.window.focusedWindow():focusWindowSouth()
    --else
        --hs.alert.show("No active window")
    --end
--end)

--hs.hotkey.bind(hyper, 'L', function()
    --if hs.window.focusedWindow() then
    --hs.window.focusedWindow():focusWindowEast()
    --else
        --hs.alert.show("No active window")
    --end
--end)

--hs.hotkey.bind(hyper, 'H', function()
    --if hs.window.focusedWindow() then
        --hs.window.focusedWindow():focusWindowWest()
    --else
        --hs.alert.show("No active window")
    --end
--end)


-- move windows
hs.hotkey.bind(hyper_shift, 'H', hs.grid.pushWindowLeft)
hs.hotkey.bind(hyper_shift, 'J', hs.grid.pushWindowDown)
hs.hotkey.bind(hyper_shift, 'K', hs.grid.pushWindowUp)
hs.hotkey.bind(hyper_shift, 'L', hs.grid.pushWindowRight)

-- resize windows
hs.hotkey.bind(hyper_shift, 'Y', hs.grid.resizeWindowThinner)
hs.hotkey.bind(hyper_shift, 'U', hs.grid.resizeWindowShorter)
hs.hotkey.bind(hyper_shift, 'I', hs.grid.resizeWindowTaller)
hs.hotkey.bind(hyper_shift, 'O', hs.grid.resizeWindowWider)
