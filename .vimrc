"call pathogen#infect()
filetype off
"call pathogen#helptags()

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required! 
Plugin 'VundleVim/Vundle.vim'
Plugin 'junegunn/fzf'
Plugin 'mileszs/ack.vim'
"Bundle 'vim-scripts/Align.git'
"
"text filtering and alignment
Plugin 'godlygeek/tabular'

"file tree browser
Plugin 'scrooloose/nerdtree'
"
"Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, and more. The plugin provides mappings to easily delete, change and add such surroundings in pairs.
Plugin 'tpope/vim-surround'
"Comment stuff out
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-speeddating'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-fugitive'
" Plugin 'svermeulen/vim-easyclip'

Plugin 'valloric/youcompleteme'

Plugin 'vim-scripts/snipMate.git'
"Bundle 'vim-scripts/taglist.vim.git'
"Bundle 'vim-scripts/pyte.git'
"Bundle 'vim-scripts/asciidoc.vim.git'
"Bundle 'vim-scripts/The-NERD-Commenter.git'
"Bundle 'vim-scripts/VHDL-indent-93-syntax.git'
"Bundle 'vim-scripts/verilog_systemverilog.vim.git'
"Bundle 'vim-scripts/python.vim.git'

"fuzzy finder
Plugin 'kien/ctrlp.vim'
"Bundle 'vim-scripts/Solarized'
"Bundle 'vim-scripts/Distinguished.git'
Plugin 'vim-scripts/jellybeans.vim.git'
"Bundle 'vim-scripts/VimOutliner.git'
"Bundle 'vim-scripts/VimOrganizer.git'

"Lean & mean status/tabline for vim that's light as air.
" Plugin 'bling/vim-airline'
Plugin 'itchyny/lightline.vim' 

"Tagbar is a Vim plugin that provides an easy way to browse the tags of the current file and get an overview of its structure. It does this by creating a sidebar that displays the ctags-generated tags of the current file, ordered by their scope. This means that for example methods in C++ are displayed under the class they are defined in.
Plugin 'majutsushi/tagbar'
"
"Bundle 'vim-scripts/Auto-Pairs.git'
"Bundle 'vim-scripts/calendar.vim--Matsumoto.git'
"Bundle 'vim-scripts/Conque-Shell.git'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on


syntax on
set nocompatible
let mapleader = ","
imap jj <Esc> " Professor VIM says '87% of users prefer jj over esc', jj abrams disagrees


" Tabs **************************************************
set softtabstop=4
set shiftwidth=4
set tabstop=8
set noexpandtab
  

" indenting *********************************************
set ai " Automatically set the indent of a new line (local to buffer)
set si " smartindent (local to buffer)


" Scrollbars ******************************************************************
set sidescrolloff=2
set numberwidth=4


" Windows *********************************************************************
set equalalways " Multiple windows, when created, are equal in size
set splitbelow splitright

" Vertical and horizontal split then hop to a new buffer
:noremap <Leader>v :vsp^M^W^W<cr>
:noremap <Leader>h :split^M^W^W<cr>
:noremap <Leader>h :split^M^W^W<cr>
:noremap <Leader>h :split^M^W^W<cr>


" Cursor highlights ***********************************************************
set cursorline
"set cursorcolumn


" Searching *******************************************************************
set hlsearch  " highlight search
set incsearch  " Incremental search, search as you type
set ignorecase " Ignore case when searching 
set smartcase " Ignore case when searching lowercase


" Colors **********************************************************************
"set background=dark
syntax on " syntax highlighting

"let g:solarized_termcolors=256
""let g:solarized_termtrans=1
"let g:solarized_contrast="high"
"let g:solarized_visibility="high"
"colorscheme solarized
"colorscheme distinguished
"colorscheme ir_black
"colorscheme midnight2
"colorscheme pyte
colorscheme jellybeans
"hi Normal ctermbg=black


" Status Line *****************************************************************
set showcmd
set ruler " Show ruler
"set ch=2 " Make command line two lines high
set colorcolumn=80
"match LongLineWarning '\%120v.*' " Error format when a line is longer than 120


" Line Wrapping ***************************************************************
set nowrap
set linebreak  " Wrap at word


" File Stuff ******************************************************************
filetype plugin indent on


" Insert New Line *************************************************************
map <S-Enter> O<ESC> " awesome, inserts new line without going into insert mode
map <Enter> o<ESC>


" Invisible characters *********************************************************
set listchars=trail:�,tab:�\ 
set nolist
:noremap <Leader>i :set list!<CR> " Toggle invisible chars

" Folding
"set fdm=syntax
"set fdc=1
"set fdl=0

" Misc settings ***************************************************************
set backspace=indent,eol,start
set number " Show line numbers
set matchpairs+=<:>
set vb t_vb= " Turn off bell, this could be more annoying, but I'm not sure how


set hidden
nnoremap ' `
nnoremap ` '
"runtime macros/matchit.vim
set wildmenu
set wildmode=list:longest,full
set confirm
set autochdir
set title
set showmatch
set mouse=a
"set guifont=Lucida_Console:h8:cANSI
"set guifont=ProggyCleanTTSZ\ 12
"set guifont=Menlo\ Regular:h11
set guifont=Monospace\ 10
set whichwrap=b,s,<,>,[,]
set guioptions-=T
set scrolloff=3
set laststatus=2
"set virtualedit=insert,onemore
set virtualedit=all
set backupdir=~/.vim/vimswap
set directory=~/.vim/vimswap
set shortmess=atI
inoremap <Left>  <NOP>
inoremap <Right> <NOP>
inoremap <Up>    <NOP>
inoremap <Down>  <NOP>


"function! MakeSession()
    "let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
    "if (filewritable(b:sessiondir) != 2)
        "exe 'silent !mkdir -p ' b:sessiondir
        "redraw!
    "endif
    "let b:filename = b:sessiondir . '/session.vim'
    "exe "mksession! " . b:filename
"endfunction

"function! LoadSession()
    "let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
    "let b:sessionfile = b:sessiondir . "/session.vim"
    "if (filereadable(b:sessionfile))
        "exe 'source ' b:sessionfile
    "else
        "echo "No session loaded."
    "endif
"endfunction

"au VimEnter * nested :call LoadSession()
"au VimLeave * :call MakeSession()

nmap <Leader>nn :NERDTreeToggle<CR>

au! BufRead,BufWrite,BufWritePost,BufNewFile *.org 
au BufEnter *.org            call org#SetOrgFileType()


"split navigation
"source: http://robots.thoughtbot.com/post/48275867281/vim-splits-move-faster-and-more-naturally
"Easier split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright


nmap <C-C> :b#<bar>bd#<CR>

" used with neovim's :term
if exists(':tnoremap')
    :tnoremap <Esc> <C-\><C-n>
endif

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
